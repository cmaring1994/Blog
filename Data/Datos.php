<?php
require('Data/Configuracion.php'); 
/**
*  CONEXION A LA BASE DE DATOS
*/
class Datos
{
	private $conexion;

	function __construct(){} //Constructor

    public function conexion(){
    	$Configuracion = new Configuracion();
    	$this->conexion = new mysqli($Configuracion->getHost(), $Configuracion->getUser(),$Configuracion->getPassword(), $Configuracion->getBD());
    	if ($this->conexion -> connect_errno) {
          die( "Fallo la conexión a MySQL: (" . $this->conexión-> mysqli_connect_errno() 
              . ") " . $this->conexión-> mysqli_connect_error());
        }
        else
        return $this->conexion;
    }

    public function cerrar($conexion){
    	try{
        mysqli_close($conexion);
        return true;
        }catch(Exception $ex){
           return false;
        }  
    }
}
 ?>