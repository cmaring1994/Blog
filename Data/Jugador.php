<?php 
/**
* 
*/
require_once("Data/MetaDatos.php");
class Jugador
{
	private $ID_JUGADOR, $NOMBRE, $RUTA, $FECHA_REG, $ACTIVO;
	function __construct($id)
	{
	   $this->ID_JUGADOR = $id;
	}

	public function Disponible(){
	  $datos;
      $MetaDatos = new MetaDatos();
      $datos = $MetaDatos->getPlayer($this->ID_JUGADOR);


         $this->addData($datos);
         if($this->ACTIVO == 1){
          return true;
         }else{
          return false;
         }

	}
    public function getID(){
		return $this->ID_JUGADOR;
	}
	public function getNombre(){
		return $this->NOMBRE;
	}
	public function getImagen(){
       return $this->RUTA;
	}
	public function getActivo(){
		return $this->ACTIVO;
	}
	public function addActivo($estado){
		$datos;
		if($estado == true){
           $MetaDatos = new MetaDatos();
           $datos = $MetaDatos->addActivoPlayer($this->ID_JUGADOR, 1);

		}else{
           $MetaDatos = new MetaDatos();
           $datos = $MetaDatos->addActivoPlayer($this->ID_JUGADOR, 0);
		}

		if($datos == 1){
			return true;
		}else{
			return false;
		}

	}

	private function addData($datos){
      $this->ID_JUGADOR = $datos[0];
      $this->NOMBRE = $datos[1];
      $this->RUTA = $datos[2];
      $this->FECHA_REG = $datos[3];
      $this->ACTIVO = $datos[4];
	}
}

 ?>