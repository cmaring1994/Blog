<?php 
require('Data/Datos.php'); 
/**
* 
*/
class MetaDatos
{
	private $datos;

    function __construct()
	{
       $this->datos = new Datos();
	}
    
    //Funcion para Validar Usuario
	public function IsExist($username, $password){
      $id;
      $conexion = $this->datos->conexion();
      $resultado = $conexion->query("CALL EPS_USUARIO('".$username."', '".$password."')");
      while($fila = $resultado->fetch_assoc()){
         $id=$fila['ID_USUARIO'];
      }
      if(empty($id)){
        $id = 0;
      }
      $this->datos->cerrar($conexion);
      return $id;
	}
    
    //Funcion Extraemos todos los datos del Usuario
	public function getDataUser($ID){
	  $datos;	
	  $conexion = $this->datos->conexion();
      $resultado = $conexion->query("CALL EPS_USUARIO_ID(".$ID.")");
      while($fila = $resultado->fetch_assoc()){
         $datos[0]=$fila['ID_USUARIO'];
         $datos[1]=$fila['NOMBRE'];
         $datos[2]=$fila['APELLIDOS'];
         $datos[3]=$fila['USUARIO'];
         $datos[4]=$fila['CONTRASENA'];
         $datos[5]=$fila['FECHA_REG'];
         $datos[6]=$fila['ACTIVO'];
      }
       $this->datos->cerrar($conexion);
      return $datos;
	}
 
  //Funcion donde extraemos la imagen del Usuario si es que tiene
  public function getImagen($ID){
      $ruta;
      $conexion = $this->datos->conexion();
      $resultado = $conexion->query("CALL EPS_USUARIO_IMAGEN(".$ID.")");
      while($fila = $resultado->fetch_assoc()){
       $ruta = $fila['RUTA'];
      }
      if(empty($ruta)){
       $ruta = "img/usuario.jpeg";
      }
       $this->datos->cerrar($conexion);
       return $ruta;
  }

  //Funcion donde extraemos los datos del JUGADOR
  public function getPlayer($ID){
    $datos;  
    $conexion = $this->datos->conexion();
      $resultado = $conexion->query("CALL EPS_JUGADOR(".$ID.")");
      while($fila = $resultado->fetch_assoc()){
         $datos[0]=$fila['ID_JUGADOR'];
         $datos[1]=$fila['NOMBRE'];
         $datos[2]=$fila['RUTA'];
         $datos[3]=$fila['FECHA_REG'];
         $datos[4]=$fila['ACTIVO'];
      }
       $this->datos->cerrar($conexion);
      return $datos;
  }

  //Funcion de extraemos los ID de los Jugadores Disponibles
  public function getPlayers(){
    $datos; 
    $contador = 0;
    $conexion = $this->datos->conexion();
    $resultado = $conexion->query("CALL EPS_JUGADORES_ACTIVOS()");
    $this->datos->cerrar($conexion);
     while($fila = $resultado->fetch_assoc()){
         $datos[$contador]=$fila['ID_JUGADOR'];
         $contador = $contador + 1;
      }
      if(empty($datos)){
         $datos[0] = 0;
      }
    return $datos;
  }

  public function getPlayersALL(){
    $datos; 
    $contador = 0;
    $conexion = $this->datos->conexion();
    $resultado = $conexion->query("CALL EPS_ALL_ID_JUGADORES()");
    $this->datos->cerrar($conexion);
     while($fila = $resultado->fetch_assoc()){
         $datos[$contador]=$fila['ID_JUGADOR'];
         $contador = $contador + 1;
      }
      if(empty($datos)){
         $datos[0] = 0;
      }
    return $datos;
  }
  //Funcion para cambiar el estado de ACTIVO al jugador
  public function addActivoPlayer($id, $activo){
    $datos; 
    $conexion = $this->datos->conexion();
    $resultado = $conexion->query("CALL EPI_UPDATE_ACTIVO_PLAYERS(".$id.",".$activo.")");
     while($fila = $resultado->fetch_assoc()){
        $datos = $fila['UNO'];
      }
      if(empty($datos)){
        $datos = 0;
      }
      $this->datos->cerrar($conexion);
    return $datos;
  }
 
}

 ?>