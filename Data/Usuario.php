<?php
require('Data/MetaDatos.php'); 
/**
* 
*/
class Usuario
{
	private $ID, $NOMBRE, $APELLIDOS, $USUARIO, $CONTRASENA, $FECHA_REG, $ACTIVO, $RUTA;
	private $JUGADORES, $ALL_JUGADORES; 

	function __construct($username, $password)
	{
		$this->USUARIO = $username;
		$this->CONTRASENA = $password;
	}//END CONSTRUCTOR

	public function Validar(){

      $MetaDatos = new MetaDatos();
      $this->ID = $MetaDatos->IsExist($this->USUARIO, $this->CONTRASENA);

      if($this->ID != 0){

      	$datos = $MetaDatos->getDataUser($this->ID);
      	$path = $MetaDatos->getImagen($this->ID);
        $players = $MetaDatos->getPlayers();
        $all_players = $MetaDatos->getPLayersALL();

        $this->addData($datos);
      	$this->addRuta($path);
        $this->addJugadores($players);
        $this->addJugadoresALL($all_players);

        return true;
      }else{

      	return false;

      }    

	}//END VALIDAR
    public function getID(){
    	return $this->ID;
    }

	public function getNombre(){
		return $this->NOMBRE;
	}
    public function getApellidos(){
		return $this->APELLIDOS;
	}
	public function getUsuario(){
		return $this->USUARIO;
	}
	public function getFechaReg(){
		return $this->FECHA_REG;
	}
	public function getActivo(){
		return $this->ACTIVO;
	}
	public function getImagen(){
		return $this->RUTA;
	}
	public function getJugadores(){
		return $this->JUGADORES;
	}
	public function getJugadoresALL(){
		return $this->ALL_JUGADORES;
	}
     //Llenamos los atributos de la clase Usuario
	private function addData($datos){

        $this->ID = $datos[0];
      	$this->NOMBRE = $datos[1];
        $this->APELLIDOS = $datos[2];
        $this->USUARIO = $datos[3];
        $this->CONTRASENA = $datos[4];
        $this->FECHA_REG = $datos[5];
        $this->ACTIVO = $datos[6];

	}

	private function addJugadores($datos){
      $this->JUGADORES = $datos;
	}
    private function addJugadoresALL($datos){
      $this->ALL_JUGADORES = $datos;
	}

	private function addRuta($ruta){
		$this->RUTA = $ruta;
	}

}
 ?>