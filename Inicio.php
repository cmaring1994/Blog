<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Inicio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="js/jquery-3.1.0.js"></script>
        <script type="text/javascript" src="js/semantic.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="css/mystye.css">
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/components/icon.min.css'>
        
     <?php 
     session_start(); 
     require('Data/Usuario.php');
     require('Data/Jugador.php');

     $ruta1 = "img/usuario2.png";
     

      if(isset($_SESSION['USUARIO'])){
          $usuario = unserialize($_SESSION['USUARIO']);
          $usuario->Validar();
          $players = $usuario->getJugadores();
          $jugadores;
          if($players[0] != 0){
            for($i=0; $i<sizeof($players); $i++){
             $clase = new Jugador($players[$i]);
             $clase->Disponible();
             $jugadores[$i] = $clase;
            }
          }else{
           $jugadores[0] = 0;
          }
      }else{
        header('Location: index.php');
      }
      ?>

    </head>

    <body>


   <div class="ui mini menu">
 <div class="ui simple dropdown item">
    Seleccione una Categoria
   <div class="menu">
        <a href="Inicio.php" class="item">Inicio</a>
        <div class="divider"></div>
        <a  href="javascript:void(0)" class="item">Jugadores</a>
        <div class="divider"></div>
        <a  href="javascript:void(0)" class="item">Nuevo Jugador</a>
        <div class="divider"></div>
        <?php 
        if($usuario->getID() == 1){
          echo " <a href='JuegoRandom.php' class='item'>Aplicaciones</a>";
        }else{
          echo " <a href='javascript:void(0)' class='item'>Aplicaciones</a>";
        }
         ?>
      </div>

 </div>
 <a href="#" class="item"><h3><?php echo $usuario->getNombre(); ?></h3></a>

  <div class="right menu">
    <a href="configuracion_abandonar.php" class="item">CERRAR SESSION</a>
      <div class="ui simple dropdown item">
         <img src="<?php echo $usuario->getImagen(); ?>" class="ui mini circular image">
          <i class="dropdown icon"></i>
          <div class="menu">
            <a class="item">Un Enlace</a>
         <div class="divider"></div>
        <a class="item">Dos Enlaces</a>
          </div>
      </div>
  </div>
</div>


<div class="html ui top attached segment">
 <div class="ui huge breadcrumb">
   <a class="section">Home</a>
   <i class="right chevron icon divider"></i>
   <a class="section">Registration</a>
   <i class="right chevron icon divider"></i>
   <div class="active section">Personal Information</div>
 </div>
  <h3 class="ui block header">Block Header </h3>
  <div class="ui segment">Segmento</div>
  <div class="ui button">Button</div>
  <div class="ui menu">
      <div class="item">One</div>
      <div class="item">Two</div>
    </div>
    <table class="ui definition table">
      <thead>
        <tr><th></th>
        <th>Header 1</th>
        <th>Header 2</th>
      </tr></thead>
      <tbody>
        <tr>
          <td>Definition</td>
          <td>1A</td>
          <td>1B</td>
        </tr>
        <tr>
          <td>Definition</td>
          <td>2A</td>
          <td>2B</td>
        </tr>
      </tbody>
    </table>
    
  </div>

 <script type="text/javascript">

  
   

 </script>
 
   
  
    </body>
</html>
