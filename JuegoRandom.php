<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Inicio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="css/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="css/mystye.css">

        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/components/icon.min.css'>

        <script type="text/javascript" src="js/semantic.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.1.0.js"></script>
     <?php 
     session_start(); 
     require('Data/Usuario.php');
     require('Data/Jugador.php');

     $ruta1 = "img/usuario2.png";
     

      if(isset($_SESSION['USUARIO'])){
          $usuario = unserialize($_SESSION['USUARIO']);
          $usuario->Validar();
          $players = $usuario->getJugadores();
          $jugadores;
          if($players[0] != 0){
            for($i=0; $i<sizeof($players); $i++){
             $clase = new Jugador($players[$i]);
             $clase->Disponible();
             $jugadores[$i] = $clase;
            }
          }else{
           $jugadores[0] = 0;
          }
      }else{
        header('Location: index.php');
      }
      ?>

    </head>

    <body>


   <div class="ui mini menu">
 <div class="ui simple dropdown item">
    Seleccione una Categoria
      <i class="dropdown icon"></i>
        <div class="menu">
        <a href="Inicio.php" class="item">Inicio</a>
        <div class="divider"></div>
        <a href="Jugadores.php" class="item">Jugadores</a>
        <div class="divider"></div>
        <a href="NuevoJugador.php" class="item">Nuevo Jugador</a>
        <div class="divider"></div>
         <a class="item">Aplicaciones</a>
      </div>

 </div>
 <a href="#" class="item"><h3><?php echo $usuario->getNombre(); ?></h3></a>

  <div class="right menu">
    <a href="configuracion_abandonar.php" class="item">CERRAR SESSION</a>
      <div class="ui simple dropdown item">
         <img src="<?php echo $usuario->getImagen(); ?>" class="ui mini circular image">
          <i class="dropdown icon"></i>
          <div class="menu">
            <a class="item">Un Enlace</a>
         <div class="divider"></div>
        <a class="item">Dos Enlaces</a>
          </div>
      </div>
  </div>
</div>

 
 <br/>
 <br/>
 <br/>

 <div class="ui container">
   <div class="ui raised very padded segment">

    <div class="ui three column grid">
      <div class="column">
      <div class="ui fluid card">
    <div class="image" id="slider">
    <?php 
      
          $cadena = "";
          $bandera = 0;
          $numero = 0;
           for($i=0; $i<sizeof($jugadores); $i++){
            $PLAYER = $jugadores[$i];
            if($bandera == 0){
             $cadena .="<img src='".$PLAYER->getImagen()."' id='imagen".$i."'> ";
             $bandera = 1;
             }else{
             $cadena .="<img src='".$PLAYER->getImagen()."' id='imagen".$i."' style='display: none'> ";
             }
             $numero = $i;
           }
           echo $cadena;
  
     ?>
    </div>
    <div class="content">
      <div class="header">JUGADOR 1</div>
      <div class="meta">
        <a>Jugador 1</a>
      </div>
      
    </div>

     </div>
     </div>
     <div class="column">
       <div class="ui fluid card">
        <div class="image">
      <img src="img/versus.png">
    </div>
        </div>
     </div>
<div class="column">
     <div class="ui fluid card">
    <div class="image" id="slider2">

       <?php 
        
          $cadena2 = "";
          $bandera = 0;
           for($i=0; $i<sizeof($jugadores); $i++){
            $PLAYER = $jugadores[$i];
            if($bandera == 0){
             $cadena2 .="<img src='".$PLAYER->getImagen()."' id='imagen2".$i."'> ";
             $bandera = 1;
             }else{
             $cadena2 .="<img src='".$PLAYER->getImagen()."' id='imagen2".$i."' style='display: none'> ";
             }
             
           }
           echo $cadena2;
     ?>

    </div>
    <div class="content">
      <div class="header">JUGADOR 2</div>
      <div class="meta">
        <span class="date">Jugador 2</span>
      </div>
      
    </div>

  </div>
</div>

</div>
<br/>
<div class="ui inverted segment">
<button class="fluid huge ui inverted red button" onClick="iniciar(<?php echo sizeof($jugadores); ?>)">RANDOM</button>
<div>
    
   </div>
 </div>
   
   <script type="text/javascript" src="js/main.js"></script>
   <script type="text/javascript">
  //document.getElementById('imagen2').style.display='none';

   function iniciar(numero){
    var n1 = aleatorio1(numero);
    var n2 = aleatorio2(numero);
     //alert(n1+" , "+n2);
     //alert(numero);
     if(n1 == n2){
        iniciar(numero);
     }
    }

    function aleatorio1(numero){
      var n = numero - 1;
    var player1 = Math.round(Math.random()*(0-n)+parseInt(n));
    var interval = null;
    $(function(){
      mostrar1(numero);
     $('#slider img:gt(0)').hide();
      interval = setInterval(function(){
      $('#slider img:first-child').fadeOut(0)
         .next('img').fadeIn(100)
         .end().appendTo('#slider');}, 100);
      setTimeout(function(){
          clearInterval(interval);
          ocultar1(numero);
          Jugador1(player1)
      }, 4000);
     });
    return player1;
    }
    function aleatorio2(numero){
      var n = numero - 1;
    var player2 = Math.round(Math.random()*(0-n)+parseInt(n));
    var interval = null;
    $(function(){
     mostrar2(numero);
     $('#slider2 img:gt(0)').hide();
      interval = setInterval(function(){
      $('#slider2 img:first-child').fadeOut(0)
         .next('img').fadeIn(100)
         .end().appendTo('#slider2');}, 100);
      setTimeout(function(){
          clearInterval(interval);
          ocultar2(numero);
          Jugador2(player2)
      }, 4000);
          
     });
     return player2;
    }
    function Jugador1(numero){
        document.getElementById('imagen'+numero).style.display='block';
    }
    function Jugador2(numero){
        document.getElementById('imagen2'+numero).style.display='block';
    }
    function mostrar1(numero){
      for(i = 1; i<numero; i++){
           document.getElementById('imagen'+i).style.display='block';
      }
    /*document.getElementById('imagen1').style.display='block';
    document.getElementById('imagen2').style.display='block';
    document.getElementById('imagen3').style.display='block';
    document.getElementById('imagen4').style.display='block';
    document.getElementById('imagen5').style.display='block';
    document.getElementById('imagen6').style.display='block';
    document.getElementById('imagen7').style.display='block';*/
    }
    function mostrar2(numero){
        for(i = 1; i<numero; i++){
           document.getElementById('imagen2'+i).style.display='block';
      }
    /*document.getElementById('imagen21').style.display='block';
    document.getElementById('imagen22').style.display='block';
    document.getElementById('imagen23').style.display='block';
    document.getElementById('imagen24').style.display='block';
    document.getElementById('imagen25').style.display='block';
    document.getElementById('imagen26').style.display='block';
    document.getElementById('imagen27').style.display='block';*/
    }
    function ocultar1(numero){
      for(i = 0; i<numero; i++){
           document.getElementById('imagen'+i).style.display='none';
      }
    /*document.getElementById('imagen0').style.display='none';
    document.getElementById('imagen1').style.display='none';
    document.getElementById('imagen2').style.display='none';
    document.getElementById('imagen3').style.display='none';
    document.getElementById('imagen4').style.display='none';
    document.getElementById('imagen5').style.display='none';
    document.getElementById('imagen6').style.display='none';
    document.getElementById('imagen7').style.display='none';*/
    }
    function ocultar2(numero){
       for(i = 0; i<numero; i++){
           document.getElementById('imagen2'+i).style.display='none';
      }
    /*document.getElementById('imagen20').style.display='none';
    document.getElementById('imagen21').style.display='none';
    document.getElementById('imagen22').style.display='none';
    document.getElementById('imagen23').style.display='none';
    document.getElementById('imagen24').style.display='none';
    document.getElementById('imagen25').style.display='none';
    document.getElementById('imagen26').style.display='none';
    document.getElementById('imagen27').style.display='none';*/
    }
    </script>
    </body>
</html>
