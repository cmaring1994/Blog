<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Listado de Jugadores</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="css/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="css/mystye.css">

        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/components/icon.min.css'>

        <script type="text/javascript" src="js/semantic.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.1.0.js"></script>
     <?php 
     session_start(); 
     require('Data/Usuario.php');
     require('Data/Jugador.php');

     $ruta1 = "img/usuario2.png";
     

      if(isset($_SESSION['USUARIO'])){
          $usuario = unserialize($_SESSION['USUARIO']);
          $allJugadores = $usuario->getJugadoresALL();
      }else{
        header('Location: index.php');
      }
      ?>

    </head>

    <body>


   <div class="ui mini menu">
 <div class="ui simple dropdown item">
    Seleccione una Categoria
      <i class="dropdown icon"></i>
        <div class="menu">
        <a href="Inicio.php" class="item">Inicio</a>
        <div class="divider"></div>
        <a href="Jugadores.php" class="item">Jugadores</a>
        <div class="divider"></div>
        <a href="NuevoJugador.php" class="item">Nuevo Jugador</a>
        <div class="divider"></div>
         <a class="item">Aplicaciones</a>
      </div>

 </div>
 <a href="#" class="item"><h3><?php echo $usuario->getNombre(); ?></h3></a>

  <div class="right menu">
    <a href="configuracion_abandonar.php" class="item">CERRAR SESSION</a>
      <div class="ui simple dropdown item">
         <img src="<?php echo $usuario->getImagen(); ?>" class="ui mini circular image">
          <i class="dropdown icon"></i>
          <div class="menu">
            <a class="item">Un Enlace</a>
         <div class="divider"></div>
        <a class="item">Dos Enlaces</a>
          </div>
      </div>
  </div>
</div>

 
 <br/>
 <br/>
 <br/>

 <div class="ui container">
   <div class="ui raised very padded segment">
 <div class="ui three column grid">
  <div class="column">
  </div>
  <div class="column">
    <?php 
     
     if(isset($_GET["msge"])){
       echo "<div class='ui red message'>ERROR: No fue modificado ".$_GET["msge"]."</div>";
     }
     if(isset($_GET["msga"])){
       echo "<div class='ui green message'>DATOS MODIFICADOS CORRECTAMENTE</div>";
     }
     ?>
    

    <form action="configuracion_guardar.php" method="post">
    <table class="ui very basic collapsing celled table">
  <thead>
    <tr><th>Jugador</th>
    <th>Estado</th>
  </tr></thead>
  <tbody>
  <?php 
   $cadena = "";
   for($i=0; $i<sizeof($allJugadores); $i++){
     $clase = new Jugador($allJugadores[$i]);
     $clase->Disponible();

      $cadena .= "<tr>";
       $cadena .= "<td>";
        $cadena .= "<h4 class='ui image header'>";
        $cadena .= "<img src='".$clase->getImagen()."' class='ui mini rounded image'>";
        $cadena .= "<div class='content'>".$clase->getNombre()."</div>";
        $cadena .= "</h4>";
       $cadena .= "</td>";
       $cadena .= "<td>";
       $cadena .= "<div class='ui toggle checkbox'>";
       if($clase->getActivo() == 1){
          $cadena .= "<input type='checkbox' name='jugador".$clase->getID()."' value='".$clase->getID()."' checked><label>Activo</label><br>";
       }else{
          $cadena .= "<input type='checkbox' name='jugador".$clase->getID()."' value='".$clase->getID()."' ><label>Activo</label><br>";
       }
        $cadena .= "</div>";
       $cadena .= "</td>";
      $cadena .= "</tr>";
   }

   echo $cadena;
   ?>

  </tbody>
</table>
<br/>
<button type="submit" class="medium ui primary button" >GUARDAR</button>
</form>
  </div>
  <div class="column">
  </div>

    </div>
   </div>


 </div>
   
   <script type="text/javascript" src="js/main.js"></script>

    </body>
</html>
