<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Nuevo Jugador</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="css/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="css/mystye.css">

        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/components/icon.min.css'>

        <script type="text/javascript" src="js/semantic.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.1.0.js"></script>
          <!-- First, include the Webcam.js JavaScript Library -->
        <script type="text/javascript" src="js/webcam.js"></script>
     <?php 
     session_start(); 
     require('Data/Usuario.php');
     require('Data/Jugador.php');

     $ruta1 = "img/usuario2.png";
     

      if(isset($_SESSION['USUARIO'])){
          $usuario = unserialize($_SESSION['USUARIO']);
          
      }else{
        header('Location: index.php');
      }
      ?>

    </head>

    <body>


   <div class="ui mini menu">
 <div class="ui simple dropdown item">
    Seleccione una Categoria
      <i class="dropdown icon"></i>
        <div class="menu">
        <a href="Inicio.php" class="item">Inicio</a>
        <div class="divider"></div>
        <a href="Jugadores.php" class="item">Jugadores</a>
        <div class="divider"></div>
        <a href="NuevoJugador.php" class="item">Nuevo Jugador</a>
        <div class="divider"></div>
         <a class="item">Aplicaciones</a>
      </div>

 </div>
 <a href="#" class="item"><h3><?php echo $usuario->getNombre(); ?></h3></a>

  <div class="right menu">
    <a href="configuracion_abandonar.php" class="item">CERRAR SESSION</a>
      <div class="ui simple dropdown item">
         <img src="<?php echo $usuario->getImagen(); ?>" class="ui mini circular image">
          <i class="dropdown icon"></i>
          <div class="menu">
            <a class="item">Un Enlace</a>
         <div class="divider"></div>
        <a class="item">Dos Enlaces</a>
          </div>
      </div>
  </div>
</div>

 
 <br/>
 <br/>
 <br/>

 <div class="ui container">
   <div class="ui raised very padded segment">
 <div class="ui three column grid">

  <div class="column">
<div id="results" class="ui info message">La imagen de Captura se Mostrara Aqui..</div>
  </div>
  <div class="column">
    <form class="ui form">
      <div class="field">
        <label>Nombre: </label>
        <input type="text" name="nombre" placeholder="Nombre Jugador">
      </div>
      <div class="field">
       <div class="ui checkbox">
        <input type="checkbox" name="terminos">
        <label>Acepto Terminos y Condiciones</label>
       </div>
      </div>
    <button class="ui button" type="submit">GUARDAR</button>
    </form>
  </div>

  <div class="column">
    
    <div id="my_camera"></div>
  <!-- Configure a few settings and attach camera -->
  <script language="JavaScript">
    Webcam.set({
      width: 320,
      height: 340,
      image_format: 'jpeg',
      jpeg_quality: 90,
      flip_horiz: true,
      force_flash: true
    });
    Webcam.attach( '#my_camera' );
  </script>
  
  <!-- A button for taking snaps -->
  <form>
    <br/>
    <button type="submit" onClick="take_snapshot()" class="large ui blue icon button"> <i class="camera retro icon"></i> Capturar</button>
  </form>

  </div>

    </div>
   </div>
 </div>
   

<!-- Code to handle taking the snapshot and displaying it locally -->
  <script language="JavaScript">
    function take_snapshot() {
      // take snapshot and get image data
      Webcam.snap( function(data_uri) {
        $(document).ready(function(){
    
           $("#results").removeClass("ui info message");
            
        });
        // display results in page
        document.getElementById('results').innerHTML =  
          '<img src="'+data_uri+'"/>';
      } );
    }
  </script>
    </body>
</html>
