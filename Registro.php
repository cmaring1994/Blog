<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>INICIO</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/semantic.min.css">
       
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/components/icon.min.css'>
        <script type="text/javascript" src="js/jquery-3.1.0.js"></script>
        <script type="text/javascript" src="js/semantic.min.js"></script>
            
      
        <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>

  <script type="text/javascript">

  
   
   function cerrar(){
     $(document).ready(function() {
      $('#txterror').hide("fast");
    });
    }

  </script>

  <?php 
     session_start(); 
      if(isset($_SESSION['USUARIO'])){
        if(empty($_SESSION['USUARIO'])){
        }else{
         header('Location: Inicio.php');  
        }
      }
      ?>
    </head>

    <body>
   <div class="ui middle aligned center aligned grid">

  <div class="column">
    <h2 class="ui teal image header">
      <img src="http://www.clker.com/cliparts/d/R/b/F/Y/S/soccerballwithwings.svg" class="image">
      <div class="content">
        JUGAR
      </div>
    </h2>
    <form enctype="multipart/form-data" class="ui large form" action="configuracion_registrar.php" method="post">

    

    <label>
      <?php 
         if(isset($_SESSION['ERROR'])){

          if(empty($_SESSION['ERROR'])){

          }else{
          echo "<div class='ui negative message' id='txterror'>
          <a  onClick='cerrar()'><i class='close icon'></i></a>
          <div class='header'>Error</div>
          <p>".$_SESSION['ERROR']."</p>
          </div>
          ";
          unset($_SESSION['ERROR']);
          }

         }
       ?>
    </label>

      <div class="ui stacked segment">

      <div class="two fields">
      <div class="field">
        <label>Nombre</label>
        <input placeholder="" name="nombre" type="text">
      </div>
      <div class="field">
        <label>Apellidos</label>
        <input placeholder="" name="apellidos" type="text">
      </div>
    </div>
    <div class="field">
       <label>Usuario</label>
        <input placeholder="Putito" name="usuario" type="text">
    </div>
    <div class="two fields">
      <div class="field">
        <label>Contraseña</label>
        <input placeholder="" name="pass" type="password">
      </div>
      <div class="field">
        <label>Repetir Contraseña</label>
        <input placeholder="" name="pass2" type="password">
      </div>
    </div>
    <div class="field">
       <label>Cargar Imagen de Perfil</label>
     <input type="hidden" name="MAX_FILE_SIZE" value="200000" />
     <input name="uploadedfile" type="file" />
   </div>
    <div class="field">
      <label><a id="mymodalg" href="javascript:void(0)">Ver terminos y Condiciones</a></label>
      <div class="ui checkbox">
        <input type="checkbox" name="terminos" >
        <label>Acepto Terminos y Condiciones</label>
      </div>
    </div>

        <button class="ui fluid large teal submit button" type="submit" id="entrar">REGISTRAR</button>
   <label><a href="index.php">Regresar</a></label>
      </div>
    

        

    </form>


  
  </div>

</div>


<div class="ui basic modal">
 
  <div class="header">
    
  </div>
  <div class="image content">
    <div class="image">
      <i class="archive icon"></i>
    </div>
    <div class="description">
      <p>Informacion al Cliente: </p>
      <p>Estoy deacuerdo en dar permisos a www.marinblog.esy.es para el
      manejo total de mis datos proporcionados de manera voluntaria sin fines maliciosos que afecten mi reputacion
      y mi honorabilidad</p>
    </div>
  </div>
  <div class="actions">
    <div class="fluid ui inverted buttons">
      <div class="ui ok green basic inverted button">
        <i class="checkmark icon"></i>
        Leeido
      </div>
    </div>
  </div>
</div>

   

   <script type="text/javascript">
   $(document).ready(function(){
     $("#mymodalg").click(function(){
      $('.ui.basic.modal').modal('show');
    });
   });
   </script>
    </body>
</html>
