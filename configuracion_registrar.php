<?php 
 session_start(); 
require('Data/Datos.php'); 
    //Logica para validar un usuario ya esta dentro del sistema quiere acceder a esta parte del sistema
if(isset($_SESSION['USUARIO'])){
	if(empty($_SESSION['USUARIO'])){
		header('Location: Inicio.php'); 
        }else{
        header('Location: Inicio.php');  
        }
}else{
	$_SESSION['ERROR'] = null;
	$path = "image/"; //Ruta donde guardaremos las imagenes cargadas

	//Logica para Guardar el Registro nuevos usuarios
  
       //Datos de los uusuarios
        $nombre = $_POST['nombre'];
        $apellidos = $_POST['apellidos'];     
        $usuario = $_POST['usuario'];
       
       //Datos de las Contraseñas que Ingresen
        $pass = $_POST['pass'];
        $pass2 = $_POST['pass2'];

        //Validamos las Cadenas de Textos
        if(ValidarString($nombre) == true && ValidarString($apellidos) == true && ValidarString($usuario) == true){            
          //Comprobamos que contengan informacion los password
           if(strlen($pass) > 0 && strlen($pass2) > 0){
             //Validamos que las Contraseñas sean mayor a 3 caracteres pero menos a 9 
             if( (strlen($pass) <= 9 && strlen($pass2) <= 9) &&  (strlen($pass) > 3 && strlen($pass2) > 3)){
                  //Validamos que las Contraseñas sean Iguales
                  if(strcmp ($pass , $pass2 ) == 0){
                  	//VALIDAMOS LA IMAGEN

                  	/* Este script Validamos el Tamaño de la Imagen 200KB
                  	$uploadedfile_size=$_FILES['uploadedfile'][size];
                    echo $_FILES[uploadedfile][name];
                    if ($_FILES[uploadedfile][size]>200000){
  	                 $msg=$msg."El archivo es mayor que 200KB, debes reduzcirlo antes de subirlo<BR>";
                    }*/

                    //Este script Validamos que el archivo sea imagen tipo jpeg o gif
                    if ($_FILES['uploadedfile']['type'] != "image/jpeg" && $_FILES['uploadedfile']['type'] !="image/jpg" && $_FILES['uploadedfile']['type'] !="image/png"){
                    //Error Tipo de Archivo no es Imagen
                    	$_SESSION['ERROR'] = "SOLO SE ADMITEN IMAGENES JPEG, JPG Y PNG";
                    	pantalla(false,"","");
                    
                    }else{

                     $path = $path . basename( $_FILES['uploadedfile']['name']); 
                     $nombre = limpiarNumeros(elimina_acentos(limpiarString($nombre)));
        	         $apellidos = limpiarNumeros(elimina_acentos(limpiarString($apellidos)));
        	         $usuario = elimina_acentos(limpiarString($usuario));

        	         if(Validarterminos() == true){

                        if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $path)) { 
                  	      //EXITO EN TODOS LOS DATOS SON ENVIADOS A LA BASE DE DATOS
                           //header('Location: Registro.php?msg=EXITO'); 
                        	$ID_NUEVO = newUsuario($nombre, $apellidos, $usuario, $pass);
                        	newImagen($ID_NUEVO, $path);
                        	pantalla(true, $usuario, $pass);
                        	
                        } else{
                          
                  	    //ERROR AL SUBIR EL ARCHIVO
                        //echo "Ha ocurrido un error, trate de nuevo!";
                          $_SESSION['ERROR'] = "ERROR AL CARGAR IMAGEN, INTENTE DE NUEVO";
                          pantalla(false,"","");

                        }

        	         }else{
        	         	//Error no Acepto Terminos
        	         	$_SESSION['ERROR'] = "DEBE ACEPTAR TERMINOS Y CONDICIONES..";
        	         	pantalla(false,"","");
        	         }
                     
                    }
        	        
                  }else{
                   //Error las contraseñas no son iguales
                  	$_SESSION['ERROR'] = "CONTRASEÑAS NO SON IGUALES..";
                  	pantalla(false,"","");
                  }
             }else{
                //Error la Longitud de contraseñas no es la correcta
                $_SESSION['ERROR'] = "CONTRASEÑAS DEBEN CONTENER MAS DE 3 Y MENOS DE 9 CARACTERES";
                pantalla(false,"","");
             }
            }else{
            	//ERROR NO HAY CONTRASEÑAS
            	$_SESSION['ERROR'] = "CONTRASEÑAS INVALIDAS..";
            	pantalla(false,"","");
            }

        }else{
        	 //ERROR CAMPOS VACIOS O CONTIENE PUROS NUMEROS
        	$_SESSION['ERROR'] = "POR FAVOR DE LLENAR LOS DATOS CORRECTAMNETE..";
            pantalla(false,"","");
        }   


        
}

//Funcion donde redireccionamos en caso de exito o algun Error
function pantalla($bandera,$user, $password){
	if($bandera == false){
	header('Location: Registro.php');
    }else{
    $_SESSION["EXITO"] = "USUARIO: ".$user."  PASS: ".$password;
    header('Location: index.php');
    }
}
//Funcion para agregar nuevos usuarios
function newUsuario($nombre, $apellidos, $usuario, $pass){
	$datos = new Datos();
    $id; 
    $conexion = $datos->conexion();
    $resultado = $conexion->query("CALL EPI_USUARIO('".$nombre."', '".$apellidos."', '".$usuario."', '".$pass."');");
    $resultado = $conexion->query("CALL EPS_LAST_ID(@resultado);");
    $resultado = $conexion->query("SELECT @resultado;");
    if(!$resultado){
    throw new Exception("Database Error [{$conexion->errno}] {$conexion->error}");
    $id=0;
    }else{
     while($fila = $resultado->fetch_assoc()){
        $id = $fila['@resultado'];
      }
    $datos->cerrar($conexion);
    }
    return $id;
  }
 //Funcion para agregar la ruta de la imagen del usuario en la base de Datos
function newImagen($id, $ruta){
	$datos = new Datos();
    $conexion = $datos->conexion();
    $resultado = $conexion->query("CALL EPI_IMAGEN_USUARIO(".$id.", '".$ruta."')");
    $datos->cerrar($conexion);
  }
//Validar Terminos Que se Acepten
function Validarterminos(){
	if(isset($_POST['terminos'])){
       return true;
	}else{
		return false;
	}
}

//Limpia numeros encontras en un String
function limpiarNumeros($cadena){
    return preg_replace('/[0-9]+/', '', $cadena);
}
//Limpiar numeros encontrados solos en un String
function limpiarNumerosSolos($cadena){
	//$cadena="la casa tiene el numero 25 y tiene 1025m2";
    return preg_replace('/[ ][0-9]+[ ]/', ' ', $cadena);
}
function limpiarString($texto)
{
      $textoLimpio = preg_replace('([^A-Za-z0-9])', '', $texto);	     					
      return $textoLimpio;
}

function elimina_acentos($texto){

  $texto=utf8_decode($texto);

  $con_acento = utf8_decode("ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ");

  $sin_acento = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";

    $texto= strtr($texto, $con_acento, $sin_acento);

    $texto = preg_replace("/[^A-Za-z0-9 _]/","",$texto);

    //si queremos pasar todos los carácteres a minusculas
    //$texto = strtolower(trim ($texto));

    //si queremos sustituir el espacio en blanco por -
    //$texto = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $texto);

    return $texto;
}

//Funcion para Validar un String
function ValidarString($str){

        if(strlen($str) > 0){

          if(is_numeric($str) == false) {

          	return true;
        
          }else{

          	return false;

          } 

        }else{

          return false;

        }
}


 ?>
