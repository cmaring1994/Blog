<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>INICIO</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/semantic.min.css">
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/components/icon.min.css'>
        <script type="text/javascript" src="js/semantic.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.1.0.js"></script>
        <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>

  <script type="text/javascript">
   function cerrar(){
     $(document).ready(function() {
      $('#txterror').hide("fast");
    });
    }
  </script>

  <?php 
     session_start(); 
      if(isset($_SESSION['USUARIO'])){
        if(empty($_SESSION['USUARIO'])){
        }else{
         header('Location: Inicio.php');  
        }
      }
      ?>
    </head>

    <body>
   <div class="ui middle aligned center aligned grid">

  <div class="column">
    <h2 class="ui teal image header">
      <img src="http://www.clker.com/cliparts/d/R/b/F/Y/S/soccerballwithwings.svg" class="image">
      <div class="content">
        JUGAR
      </div>
    </h2>
    <form class="ui large form" action="configuracion_login.php" method="post">

    

    <label>
      <?php 
         if(isset($_GET['msg'])){
          echo "<div class='ui negative message' id='txterror'>
          <a  onClick='cerrar()'><i class='close icon'></i></a>
          <div class='header'>Ups Tenemos Un Error.</div>
          <p>USUARIO O CONTRASEÑA INCORRECTO</p>
          </div>
          ";
         }

         if(isset($_SESSION['EXITO'])){
          if(empty($_SESSION['EXITO'])){
          }else{
          echo "<div class='ui positive message' id='txterror'>
          <a  onClick='cerrar()'><i class='close icon'></i></a>
          <div class='header'>REGISTRO EXITOSO</div>
          <p>".$_SESSION['EXITO']."</p>
          </div>
          ";
           unset($_SESSION['EXITO']);
          }
         }
         
       ?>
    </label>

      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="usuario" placeholder="Usuario">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Contraseña">
          </div>
        </div>
        <button class="ui fluid large teal submit button" type="submit" id="entrar">ACEPTAR</button>
      </div>
    

        

    </form>
    
    <div class="ui message">
      Basura Nueva? <a href="Registro.php">REGISTRO</a>
    </div>
  </div>
</div>
   <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>
